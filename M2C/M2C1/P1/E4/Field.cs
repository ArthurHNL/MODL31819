﻿namespace M2C.P1.E4
{
    class Field
    {
        public Field NeighbouringField { get; private set; }
        public Field(Field neighbour)
        {
            this.NeighbouringField = neighbour;
        }
    }
}
