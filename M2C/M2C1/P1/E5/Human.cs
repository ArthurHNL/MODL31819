﻿using System.Collections.Generic;

namespace M2C.P1.E5
{
    class Human
    {
        private readonly List<Dog> _dogs;
        public List<Dog> Dogs => _dogs;
        public Human()
        {
            _dogs = new List<Dog>();
        }
    }
}
