﻿namespace M2C.P1.E5
{
    class Dog
    {
        public string Breed { get; set; }
        public string Name { get; set; }
        public Human Boss { get; private set; }
        public Dog(Human boss)
        {
            Boss = boss;
        }
    }
}
