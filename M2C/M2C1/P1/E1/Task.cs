﻿namespace M2C.P1.E1
{
    class Task
    {
        public string Name { get; set; }
        public Part Part { get; private set; }

        public Task(Part p)
        {
            this.Part = p;
        }

    }
}
