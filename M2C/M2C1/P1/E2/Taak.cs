﻿namespace M2C.P1.E2
{
    class Taak
    {
        public string TaakNaam { get; set; }
        public Onderdeel Onderdeel { get; private set; }
        public Taak(Onderdeel o)
        {
            this.Onderdeel = o;
        }

    }
}
