﻿using System.Collections.Generic;

namespace M2C.P1.E6
{
    class Marathon
    {
        public string Location { get; set; }
        public List<Walker> Walkers => _walkers;
        private readonly List<Walker> _walkers;
        public Marathon()
        {
            _walkers = new List<Walker>();
        }
    }
}
