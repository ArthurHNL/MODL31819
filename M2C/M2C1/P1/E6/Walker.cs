﻿using System.Collections.Generic;

namespace M2C.P1.E6
{
    class Walker
    {
        private readonly List<Marathon> _marathons;
        public List<Marathon> Marathons => _marathons;
        public Walker()
        {
            _marathons = new List<Marathon>();
        }
    }
}
