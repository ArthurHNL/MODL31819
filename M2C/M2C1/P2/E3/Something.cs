﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M2C.P2.E3
{
    class Something
    {
        public static void Operation1()
        {
            Console.WriteLine("This is a static method invoked on the class!");
        }

        public void Operation2()
        {
            Console.WriteLine("This is a method invoked on a Something object!");
        }
    }
}
