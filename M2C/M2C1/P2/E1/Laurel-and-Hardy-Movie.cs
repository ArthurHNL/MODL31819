﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M2C.P2.E1
{
    class Laurel_and_Hardy_Movie
    {
        public readonly Actor Laurel;
        public readonly Actor Hardy;
        public Laurel_and_Hardy_Movie()
        {
            Laurel = new Actor("Stan Laurel");
            Hardy = new Actor("Oliver Hardy");
        }
    }
}
