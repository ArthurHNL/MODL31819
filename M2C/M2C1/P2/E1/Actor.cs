﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M2C.P2.E1
{
    class Actor
    {
        public String Name { get; private set; }
        public Actor(String name) => Name = name;
    }
}
