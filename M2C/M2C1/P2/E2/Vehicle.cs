﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M2C.P2.E2
{
    class Vehicle
    {
        public int Speed { get; protected set; }
        public int Size { get; private set; }

        public Vehicle(int size)
        {
            Speed = 0;
            Size = size;
        }
    }
}
