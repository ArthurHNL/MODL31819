﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M2C.P2.E2
{
    class Car : Vehicle
    {
        public Car(int size) : base(size)
        { 
            Speed = 30;
            // Size = 3; Gives error cause Size set is private.
        }
    }
}
