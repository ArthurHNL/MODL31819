﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MODL3_Dobbelsteen
{
    public class MysteryView
    {
        public void ShowValue(int value)
        {
            Console.Write("> MysteryView: ");
            Console.WriteLine(Convert.ToString(value, 2));
        }
    }
}