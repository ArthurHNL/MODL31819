﻿using MODL3_Dobbelsteen.Process;


namespace MODL3_Dobbelsteen
{
    class Program
    {
        static void Main(string[] args)
        {
            Controller controller = new Controller();
            controller.Go();

        }
    }
}
