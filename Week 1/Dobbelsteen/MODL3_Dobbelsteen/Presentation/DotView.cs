﻿using System;

namespace MODL3_Dobbelsteen.Presentation
{
    class DotView
    {

        public void ShowValue(int val)
        {
            Console.Write("> DotView    : ");
            for(int i = 0; (val > 0) && (i < val); i++)
            {
                Console.Write("o ");
            }
            Console.WriteLine();

        }
    }
}
