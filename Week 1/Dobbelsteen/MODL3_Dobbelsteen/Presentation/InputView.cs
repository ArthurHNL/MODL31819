﻿using System;

namespace MODL3_Dobbelsteen.Presentation
{
    class InputView
    {
        public char AskForInput()
        {
            char x = 'x';
            Console.WriteLine("--- InputView ---");

            ConsoleKeyInfo input;
            while(x != 's' && x != 'g')
            {
                Console.WriteLine("geef een opdracht ( s = stop;  g = gooi)");
                input = Console.ReadKey();
                x = input.KeyChar;
                Console.WriteLine();
                if(x != 's' && x != 'g')
                {
                    Console.WriteLine("> ?");
                }
            }
            return x;
        }

        public void ShowEnd()
        {
            ConsoleKeyInfo input;
            Console.Clear();
            Console.WriteLine("===== einde =====");
            Console.WriteLine(" (press any key) ");
            input = Console.ReadKey();
        }
    }
}
