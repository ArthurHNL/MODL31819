﻿using System;

namespace MODL3_Dobbelsteen.Presentation
{
    class WordView
    {
        public void ShowValue(int val)
        {
            string word = "onbekend";

            Console.Write("> WordView   : ");
            switch(val)
            {
                case 1:
                    word = "een";
                    break;
                case 2:
                    word = "twee";
                    break;
                case 3:
                    word = "drie";
                    break;
                case 4:
                    word = "vier";
                    break;
                case 5:
                    word = "vijf";
                    break;
                case 6:
                    word = "zes";
                    break;
            }
            Console.Write(word);
            Console.WriteLine();

        }
    }
}
