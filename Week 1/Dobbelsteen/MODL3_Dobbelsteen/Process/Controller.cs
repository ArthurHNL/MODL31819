﻿using MODL3_Dobbelsteen.Model;
using MODL3_Dobbelsteen.Presentation;

namespace MODL3_Dobbelsteen.Process
{
    class Controller
    {
        private Dobbelsteen dice;
        private InputView inputview;
        private DigitView cijferview;
        private WordView woordview;
        private DotView stippelview;
        private MysteryView mysteryView;

        public Controller()
        {
            // views
            inputview = new InputView();
            cijferview = new DigitView();
            woordview = new WordView();
            stippelview = new DotView();
            mysteryView = new MysteryView();
            // model
            dice = new Dobbelsteen();

        }

        public void Go()
        {
            char x = 'x';
            while (x != 's')
            {
                x = inputview.AskForInput();
                if (x == 'g')
                {
                    int w = dice.Throw();
                    cijferview.ShowValue(w);
                    woordview.ShowValue(w);
                    stippelview.ShowValue(w);
                    mysteryView.ShowValue(w);

                }
            }

            inputview.ShowEnd();


        }
    }
}
