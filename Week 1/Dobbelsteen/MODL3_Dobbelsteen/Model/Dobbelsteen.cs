﻿using System;

namespace MODL3_Dobbelsteen.Model
{
    class Dobbelsteen
    {

       //private int _thrownValue;  // niet nodig in C# (het is geen JAVA)
        private Random generator = new Random();

        public int ThrownValue
        {
            get;
            //{
            //    return _thrownValue; // niet nodig in C# (het is geen JAVA)
            //}
            private set;
            //{
            //    _thrownValue = value; // niet nodig in C# (het is geen JAVA)
            //}
        }

        public int Throw()
        {
            ThrownValue = generator.Next(6) + 1;
            return ThrownValue;
        }
    }
}
